import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    categories: [
      {
        id: 1,
        title: 'Backlog',
        slug: 'backlog'
      },
      {
        id: 2,
        title: 'In progress',
        slug: 'in-progress'
      },
      {
        id: 3,
        title: 'Testing',
        slug: 'testing'
      },
      {
        id: 4,
        title: 'Done',
        slug: 'done'
      },
    ],
    tasks: []
  },
  getters: {
    getTasksLength: state => categoryId =>{
      let tasks = []
      state.tasks.forEach(function(item) {
        if(item.category == categoryId){
          tasks.push(item)
        }
      });
      return tasks.length
    },
    getCategories: state => {
      return state.categories
    },
    getCategoryIdbySlug: state => slug => {
      return state.categories.find(category => category.slug === slug)
    },
    getCategoryIdbyTitle: state => title => {
      return state.categories.find(category => category.title === title)
    },
    getTasks: state => categoryId => {
      let tasks = []
      state.tasks.forEach(function(item) {
        if(item.category == categoryId){
          tasks.push(item)
        }
      });
      return tasks
    }
  },
  mutations: {
    localSave(state){
      const tasks = JSON.stringify(state.tasks);
      localStorage.setItem('tasks', tasks);
    },
    addTask(state, task){
      state.tasks.push(task)
    },
    deleteTask(state, index){
      state.tasks.splice(index, 1)
    },
    editTask(state, task){
      this.state.tasks.forEach(function(item, index) {
        if(item.id == task.id){
          item.category = task.category
        }
      })
    }
  },
  actions: {
    addTask({commit}, task){
      commit('addTask', task)
      commit('localSave')
    },
    deleteTask({commit}, id){
      let taskIndex
      this.state.tasks.forEach(function(item, index) {
        if(item.id == id){
          taskIndex = index
        }
      })
      commit('deleteTask', taskIndex)
      commit('localSave')
    },
    editTask({commit}, task){
      commit('editTask', task)
      commit('localSave')
    }
  }
})
